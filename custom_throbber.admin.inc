<?php

/**
 * @file
 * Settings form for Custom Throbber.
 */

/**
 * Create the settings page form.
 */
function custom_throbber_settings($form, &$form_state) {

  // Load the admin css to make the form prettier.
  //drupal_add_css(drupal_get_path('module', 'custom_throbber') . '/css/admin-form.css');

  // Get base site URL to display option images correctly.
  global $base_url;
  $custom_throbber_path = drupal_get_path('module', 'custom_throbber');

  // Set default image.
  $default_image = $custom_throbber_path . '/assets/images/default.gif';

  // Set additional throbber images.
  $throbber_images = file_scan_directory(drupal_get_path('module', 'custom_throbber') . '/assets/images', '/^.*\.(jpg|gif|png|png)$/');

  // Set custom directory so the user can upload custom throbber images.
  $custom_throbbers_directory = 'public://custom_throbber';

  // Format the custom directory path to display it later to the user.
  $custom_throbbers_directory_path = str_replace($base_url . '/', "", file_create_url($custom_throbbers_directory));

  // Check if the custom throbber directory exist and if not create it.
  if (!file_prepare_directory($custom_throbbers_directory, $options = FILE_CREATE_DIRECTORY)) {
    drupal_set_message(t('Notice: There was a problem creating the custom images directory. Please make sure your public folder is writable by Drupal and reinstall the module. You can also ignore this notice and use only the images provided by the module, or you can create the custom image folder manually. The folder must be named “custom_throbber” and must be placed in your public directory. If you need more help please use the module issue page or read the documentation.'), 'warning', TRUE);
  }

  // Scan for custom throbber images (if any).
  $custom_throbber_images = file_scan_directory($custom_throbbers_directory_path, '/^.*\.(jpg|gif|png|png)$/');

  // Set default options for "$form['image']['custom_throbber_image']".
  $image_options = array(
    $default_image => theme('image', array('path' => $default_image)),
  );

  // Add all images to the radio form.
  foreach (array_merge($throbber_images, $custom_throbber_images) as $image_file) {
    $image_file_url = file_create_url($image_file->uri);
    $image_options[$image_file->uri] = theme('image', array('path' => $image_file_url));
  }

  $form['image'] = array(
    '#type' => 'fieldset',
    '#title' => t('Throbber Image'),
  );

  $form['image']['custom_throbber_image'] = array(
    '#type' => 'radios',
    '#title' => t('Select throbber image'),
    '#default_value' => variable_get('custom_throbber_image', $default_image),
    '#options' => $image_options,
    '#description' => t('Do you want to add your own images? Upload them to <span class="path">@custom_throbbers_directory</span> and refresh this page.', array(
      '@custom_throbbers_directory' => $custom_throbbers_directory_path,
      )
    ),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Submit handler for custom_throbber_settings().
 */
function custom_throbber_settings_submit($form, &$form_state) {

  // Define $base_url.
  global $base_url;

  // Get image info.
  $image_info = image_get_info($form_state['values']['custom_throbber_image']);

  // Shuld we add the "div.view" css prefix?
  $css_views_prefix = '';

  // Should we perpend !important?
  $css_important = ($form_state['values']['custom_throbber_append_important'] == TRUE ? ' !important' : NULL);

  // Generate the CSS file.
  $css = '' . $css_views_prefix . 'div.ajax-progress {
  background: rgba(' . $form_state['values']['custom_throbber_background'] . ',' . $form_state['values']['custom_throbber_opacity'] . ')' . $css_important . ';
  position: absolute' . $css_important . ';
  top: 50%' . $css_important . ';
  right: 0.75em' . $css_important . ';
  margin-top: -' . $image_info['height'] * 0.6 . 'px' . $css_important . ';
  height: ' . $image_info['height'] . 'px;
  width: ' . $image_info['width'] . 'px;
  z-index: 9998' . $css_important . ';
}

#views-ui-list-page .ajax-progress-throbber,
.views-admin .ajax-progress-throbber,
#views-ajax-popup .ajax-progress-throbber {
  height: ' . $image_info['height'] * 1.5 . 'px;
  width: ' . $image_info['width'] * 1.5 . 'px;
}

' . $css_views_prefix . '.ajax-progress div.throbber {
  background: url("' . $base_url . '/' . $form_state['values']['custom_throbber_image'] . '") no-repeat scroll 50% 50%' . $css_important . ';
  background-color: ' . ($form_state['values']['custom_throbber_image_background'] ? 'rgb(' . $form_state['values']['custom_throbber_background'] . ')' : 'transparent') . '' . $css_important . ';
  border-radius: ' . ($form_state['values']['custom_throbber_image_background_use_border_radius'] ? $form_state['values']['custom_throbber_image_background_border_radius'] : '0') . 'px' . $css_important . ';
  height: ' . $image_info['height'] . 'px' . $css_important . ';
  width: ' . $image_info['width'] . 'px' . $css_important . ';
  z-index: 9999' . $css_important . ';
}
html.js input.form-autocomplete {
  background-image: none !important;
}
';

  // Format the css code output and remove some unnecessary spaces.
  $css = str_replace("	", " ", $css);

  // Write the final formated css file.
  $css_path = drupal_get_path('module', 'custom_throbber') . '/assets/css';
  if (file_prepare_directory($css_path, FILE_CREATE_DIRECTORY)) {
    file_unmanaged_save_data($css, drupal_get_path('module', 'custom_throbber') . '/assets/css/custom_throbber.css', FILE_EXISTS_REPLACE);
  }

  // Exclude unnecessary elements.
  form_state_values_clean($form_state);

  foreach ($form_state['values'] as $key => $value) {
    if (is_array($value) && isset($form_state['values']['array_filter'])) {
      $value = array_keys(array_filter($value));
    }
    variable_set($key, $value);
  }

  drupal_set_message(t('The configuration options have been saved.'));
}
